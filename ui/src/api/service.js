import axios from 'axios';

const baseEndpoint = "http://192.168.1.104:9000/";


export function Post(req, callback) {
  axios.post(baseEndpoint, req).then((res) => {
    callback(res)
  }).catch(err => {
    callback(err.response)
  })
}
export function Update(id, req, callback) {
  axios.put(baseEndpoint + id, req).then((res) => {
    callback(res)
  }).catch(err => {
    callback(err.response)
  })
}

export function Delete(id, callback) {
  axios.delete(baseEndpoint + id).then((res) => {
    callback(res)
  }).catch(err => {
    callback(err.response)
  })
}


export function GetById(id, callback) {
  axios.get(baseEndpoint + id).then((res) => {
    callback(res)
  }).catch(err => {
    callback(err.response)
  })
}
export function Get(callback) {
  axios.get(baseEndpoint).then((res) => {
    callback(res)
  }).catch(err => {
    callback(err.response)
  })
}
export function GetFilter(type, callback) {
  if (type)
    axios.get(baseEndpoint + "?filter=" + type).then((res) => {
      callback(res)
    }).catch(err => {
      callback(err.response)
    })
  else
    Get(callback)
}

export function Check(lang, id, req, callback) {
  axios.put(baseEndpoint + lang + "/" + id, req).then((res) => {
    callback(res)
  }).catch(err => {
    callback(err.response)
  })
}
