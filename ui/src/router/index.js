import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  
  // {
  //   path: "/",
  //   name: "Home",
  //   component: Dictionary,
  //   props: {
  //     lang: "tr"
  //   }
  // },
  // {
  //   path: "/turkish",
  //   name: "Turkish",
  //   component: () =>
  //     import( /* webpackChunkName: "buy" */ "../views/Dictionary.vue"),
  //   props: {
  //     lang: "tr"
  //   }
  // },
  // {
  //   path: "/english",
  //   name: "English",
  //   component: () =>
  //     import( /* webpackChunkName: "buy" */ "../views/Dictionary.vue"),
  //   props: {
  //     lang: "en"
  //   }
  // },
  // {
  //   path: "/tr/new",
  //   name: "Turkish",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import( /* webpackChunkName: "about" */ "../views/New.vue"),
  //   props: {
  //     lang: "tr"
  //   }
  // },
  // {
  //   path: "/en/new",
  //   name: "English",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import( /* webpackChunkName: "about" */ "../views/New.vue"),
  //   props: {
  //     lang: "en"
  //   }
  // }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
