const WordTypes = [{
    id: "noun",
    text: "Noun(İsim)"
  },
  {
    id: "adverb",
    text: "Adverb(Zarf)"
  },
  {
    id: "adjective",
    text: "Adjective(Sıfat)"
  },
  {
    id: "verb",
    text: "Verb(Fiil)"
  },
  {
    id: "preposition",
    text: "Preposition(Edat)"
  },
  {
    id: "pronoun",
    text: "Pronoun(Zamir)"
  },
  {
    id: "conjunction",
    text: "Conjunction(Bağlaç)"
  },
  {
    id: "others",
    text: "Others"
  },
  
]


const GetName = (id) => {
  return WordTypes.find(x => x.id == id)?.text
}

export default {
  WordTypes,
  GetName
}
