module gitlab.com/TechFundy/english-dictionary/api

go 1.16

require (
	github.com/labstack/echo/v4 v4.5.0
	go.mongodb.org/mongo-driver v1.7.2
	gopkg.in/yaml.v2 v2.4.0
)
