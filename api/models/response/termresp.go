package response

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type TermResp struct {
	Id          primitive.ObjectID `json:"id"`
	Turkish     string             `json:"turkish"`
	English     string             `json:"english"`
	Type        string             `json:"type"`
	Description string             `json:"description"`
	Count       int64              `json:"count"`
}
