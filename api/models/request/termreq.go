package request

import (
	"strings"

	"gitlab.com/TechFundy/english-dictionary/api/common"
)

type TermReq struct {
	Turkish     string `json:"turkish"`
	English     string `json:"english"`
	Description string `json:"description"`
	Type        string `json:"type"`
}

func (t *TermReq) Value(l common.Lang) string {
	if l == common.English {
		return t.English
	}
	return t.Turkish
}

func (t *TermReq) TurkishWords() []string {
	arr := strings.Split(t.Turkish, ",")
	var newArray []string
	for _, text := range arr {
		text = common.TrimToLower(text)
		if text != "" {
			newArray = append(newArray, text)
		}
	}
	return newArray

}
func (t *TermReq) EnglishWord() string {
	return common.TrimToLower(t.English)
}
