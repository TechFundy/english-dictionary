package entities

import (
	"strings"

	"gitlab.com/TechFundy/english-dictionary/api/common"
	"gitlab.com/TechFundy/english-dictionary/api/models/request"
	"gitlab.com/TechFundy/english-dictionary/api/models/response"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DictionaryEntity struct {
	Id          primitive.ObjectID `bson:"_id"`
	English     string             `bson:"english"`
	Turkish     []string           `bson:"turkish"`
	Description string             `bson:"description"`
	Type        string             `bson:"type"`
	Count       int64              `bson:"count"`
}

func NewDictionary(tr []string, en, wordType, description string) DictionaryEntity {
	return DictionaryEntity{Id: primitive.NewObjectID(), English: en, Turkish: tr, Type: wordType, Description: description, Count: 0}
}

func (d *DictionaryEntity) increment() {
	d.Count++
}
func (d *DictionaryEntity) HasValue(turkishWords *[]string) {
	var lastArray []string
	for _, text := range *turkishWords {
		if len(d.Turkish) == 0 {
			lastArray = append(lastArray, text)
		} else {
			var exist bool = false
			for _, word := range d.Turkish {
				if word == text {
					exist = true
				}
			}
			if !exist {
				lastArray = append(lastArray, text)
			}
		}
	}
	for _, word := range lastArray {
		d.Turkish = append(d.Turkish, word)
	}
}

func (d *DictionaryEntity) decrement() {
	d.Count--
}

func (d *DictionaryEntity) CheckTR(v string) {
	if d.English != common.TrimToLower(v) {
		d.increment()
	} else {
		d.decrement()
	}
}
func (d *DictionaryEntity) CheckEN(v string) {
	var exist bool = false
	for _, text := range d.Turkish {
		if text == common.TrimToLower(v) {
			exist = true
		}

	}
	if !exist {
		d.increment()
	} else {
		d.decrement()
	}
}
func (d *DictionaryEntity) Update(req *request.TermReq) {
	d.English = req.English
	d.Turkish = req.TurkishWords()
	d.Type = req.Type
	d.Description = req.Description
}
func (d *DictionaryEntity) ToTermResp() response.TermResp {
	return response.TermResp{Id: d.Id, Turkish: strings.Join(d.Turkish, ","), English: d.English, Count: d.Count, Description: d.Description, Type: d.Type}
}
