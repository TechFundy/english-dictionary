package services

import (
	"context"
	"errors"

	"gitlab.com/TechFundy/english-dictionary/api/models/entities"
	"gitlab.com/TechFundy/english-dictionary/api/models/request"
	"gitlab.com/TechFundy/english-dictionary/api/models/response"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type DictionaryService struct {
	collection *mongo.Collection
}

func NewDictionaryService(col *mongo.Collection) DictionaryService {
	return DictionaryService{collection: col}
}
func (d *DictionaryService) Delete(id string) error {
	newId, _ := primitive.ObjectIDFromHex(id)
	_, err := d.collection.DeleteOne(context.Background(), bson.M{"_id": newId})
	return err
}
func (d *DictionaryService) CheckTR(id, value string) (int64, error) {
	entity := d.FindOne(id)

	entity.CheckTR(value)
	err := d.Replace(entity)
	return entity.Count, err
}
func (d *DictionaryService) CheckEN(id, value string) (int64, error) {
	entity := d.FindOne(id)
	entity.CheckEN(value)
	err := d.Replace(entity)
	return entity.Count, err
}

func (d *DictionaryService) SaveIfNotExist(req *request.TermReq) error {
	turkishWords := req.TurkishWords()
	existEntity := d.Exists(&turkishWords, req.EnglishWord(), req.Type)
	if len(turkishWords) == 0 {
		return errors.New("Term already exists")
	}
	if existEntity.English != "" {
		err := d.Replace(existEntity)
		return err
	} else {
		err := d.Save(turkishWords, req.EnglishWord(), req.Type, req.Description)
		return err
	}
}
func (d *DictionaryService) Update(id string, req *request.TermReq) error {
	entity := d.FindOne(id)
	entity.Update(req)
	return d.Replace(entity)
}

func (d *DictionaryService) Exists(turkish *[]string, english, wordType string) entities.DictionaryEntity {
	entity := entities.DictionaryEntity{}
	d.collection.FindOne(context.Background(), bson.M{"english": english, "type": wordType}).Decode(&entity)
	entity.HasValue(turkish)
	return entity
}
func (d *DictionaryService) FindOne(id string) entities.DictionaryEntity {
	entity := entities.DictionaryEntity{}
	newId, _ := primitive.ObjectIDFromHex(id)
	d.collection.FindOne(context.Background(), bson.M{"_id": newId}).Decode(&entity)
	return entity
}

func (d *DictionaryService) Save(trkish []string, english, wordType, description string) error {
	entity := entities.NewDictionary(trkish, english, wordType, description)
	_, err := d.collection.InsertOne(context.Background(), entity)
	return err
}

func (d *DictionaryService) Replace(ent entities.DictionaryEntity) error {
	_, err := d.collection.ReplaceOne(context.Background(), bson.M{"_id": ent.Id}, ent)
	return err
}
func (d *DictionaryService) GetAll(wordType string) ([]response.TermResp, error) {
	findOptions := options.Find()
	findOptions.SetSort(bson.D{{"count", -1}})
	filt := bson.M{}
	if wordType != "" {
		filt = bson.M{"type": wordType}
	}
	cursor, err := d.collection.Find(context.Background(), filt, findOptions)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.Background())
	var response []response.TermResp
	for cursor.Next(context.Background()) {
		ent := entities.DictionaryEntity{}
		cursor.Decode(&ent)
		response = append(response, ent.ToTermResp())
	}

	return response, nil
}
func (d *DictionaryService) Get(id string) (response.TermResp, error) {

	entity := d.FindOne(id)
	if entity.English == "" {
		return entity.ToTermResp(), errors.New("Not found")
	}
	return entity.ToTermResp(), nil
}
