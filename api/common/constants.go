package common

import "strings"

type Lang string

const (
	Turkish Lang = "TR"
	English Lang = "EN"
)

func (l *Lang) GetList() []Lang {
	array := []Lang{}
	array[0] = Turkish
	array[1] = English
	return array
}

func TrimToLower(str string) string {
	str = strings.TrimLeft(str, " ")
	str = strings.TrimRight(str, " ")
	str = strings.ToLower(str)

	return str

}
