package router

import (
	"context"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/TechFundy/english-dictionary/api/config"
	"gitlab.com/TechFundy/english-dictionary/api/controllers"
	"gitlab.com/TechFundy/english-dictionary/api/services"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func Install(conf config.Config) (*echo.Echo, error) {

	// Set client options
	clientOptions := options.Client().ApplyURI(conf.ConnectionString)

	// Connect to MongoDB
	client, _ := mongo.Connect(context.TODO(), clientOptions)
	err := client.Ping(context.TODO(), nil)
	if err != nil {
		panic(err)
	}

	collection := client.Database("dictionaryDB").Collection("dictionary")
	srvc := services.NewDictionaryService(collection)
	contrl := controllers.NewDictionaryApi(srvc)
	e := echo.New()
	// e.Use(Logging)
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodPost, http.MethodDelete},
	}))
	e.GET("/hello", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	e.POST("/", contrl.Post)
	e.DELETE("/:id", contrl.Delete)
	e.GET("/", contrl.GetAll)
	e.GET("/:id", contrl.Get)
	e.PUT("/:id", contrl.Update)
	e.PUT("/tr/:id", contrl.CheckTR)
	e.PUT("/en/:id", contrl.CheckEN)

	return e, nil
}
