package config

import (
	"log"
	"os"

	yaml "gopkg.in/yaml.v2"
)

type Config struct {
	Server struct {
		Port int64 `yaml:"port"`
	} `yaml:"server"`
	ConnectionString string `yaml:"connectionString"`
}

func Read(fileName string) Config {

	var cfg Config

	f, err := os.Open(fileName)
	if err != nil {
		log.Panic(err)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&cfg)
	if err != nil {
		log.Panic(err)
	}

	return cfg
}
