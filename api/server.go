package main

import (
	"fmt"

	"gitlab.com/TechFundy/english-dictionary/api/config"
	"gitlab.com/TechFundy/english-dictionary/api/router"
)

func main() {
	config := config.Read("config.yaml")
	e, err := router.Install(config)
	if err != nil {
		panic(err)
	}
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", config.Server.Port)))
}
