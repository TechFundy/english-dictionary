package controllers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/TechFundy/english-dictionary/api/models/request"
	"gitlab.com/TechFundy/english-dictionary/api/services"
)

type DictionaryController struct {
	srv services.DictionaryService
}

func NewDictionaryApi(srvc services.DictionaryService) DictionaryController {
	return DictionaryController{srv: srvc}
}

func (d *DictionaryController) Post(ctx echo.Context) error {
	req := new(request.TermReq)
	if err := ctx.Bind(req); err != nil {
		return ctx.String(http.StatusBadRequest, err.Error())
	}
	err := d.srv.SaveIfNotExist(req)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, err.Error())
	}
	return ctx.JSON(http.StatusOK, "Created Successfully")
}
func (d *DictionaryController) Update(ctx echo.Context) error {
	id := ctx.Param("id")
	req := new(request.TermReq)
	if err := ctx.Bind(req); err != nil {
		return ctx.String(http.StatusBadRequest, err.Error())
	}
	err := d.srv.Update(id, req)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, err.Error())
	}
	return ctx.JSON(http.StatusOK, "Updated Successfully")
}

func (d *DictionaryController) GetAll(ctx echo.Context) error {
	filter := ctx.QueryParam("filter")
	list, err := d.srv.GetAll(filter)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, err.Error())
	}
	return ctx.JSON(http.StatusOK, list)
}
func (d *DictionaryController) Get(ctx echo.Context) error {
	id := ctx.Param("id")
	item, err := d.srv.Get(id)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, err.Error())
	}
	return ctx.JSON(http.StatusOK, item)
}

func (d *DictionaryController) CheckTR(ctx echo.Context) error {
	req := new(request.CheckReq)
	if err := ctx.Bind(req); err != nil {
		return ctx.String(http.StatusBadRequest, err.Error())
	}
	id := ctx.Param("id")
	count, err := d.srv.CheckTR(id, req.Value)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, err.Error())
	}
	return ctx.JSON(http.StatusOK, count)
}
func (d *DictionaryController) CheckEN(ctx echo.Context) error {
	req := new(request.CheckReq)
	if err := ctx.Bind(req); err != nil {
		return ctx.String(http.StatusBadRequest, err.Error())
	}
	id := ctx.Param("id")
	count, err := d.srv.CheckEN(id, req.Value)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, err.Error())
	}
	return ctx.JSON(http.StatusOK, count)
}

func (d *DictionaryController) Delete(ctx echo.Context) error {
	id := ctx.Param("id")
	err := d.srv.Delete(id)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, err.Error())
	}
	return ctx.JSON(http.StatusOK, "")
}
